#!/usr/bin/make -f
export DH_VERBOSE = 1

%:
	dh $@ --buildsystem=pybuild

SIZES = 16 32 64 128 256 512

override_dh_auto_build:
	PYBUILD_NAME=onionshare-cli dh_auto_build --buildsystem=pybuild --sourcedirectory cli --\
	    --after-build "CURDIR=$(CURDIR) BUILD_DIR={build_dir} $(CURDIR)/debian/missing-sources/uglifyjs.sh"
	PYBUILD_NAME=onionshare dh_auto_build --buildsystem=pybuild --sourcedirectory desktop

override_dh_auto_clean:
	PYBUILD_NAME=onionshare-cli dh_auto_clean --buildsystem=pybuild --sourcedirectory cli
	PYBUILD_NAME=onionshare dh_auto_clean --buildsystem=pybuild --sourcedirectory desktop
	find . -type d -name '*.egg-info' -exec rm -rvf {} \+
	find . -type d -name '.pytest_cache' -exec rm -rvf {} \+

override_dh_auto_configure:
	PYBUILD_NAME=onionshare-cli dh_auto_configure --buildsystem=pybuild --sourcedirectory cli
	PYBUILD_NAME=onionshare dh_auto_configure --buildsystem=pybuild --sourcedirectory desktop

override_dh_auto_install:
	PYBUILD_NAME=onionshare-cli dh_auto_install --buildsystem=pybuild --destdir=debian/onionshare-cli --sourcedirectory cli
	PYBUILD_NAME=onionshare dh_auto_install --buildsystem=pybuild --destdir=debian/onionshare --sourcedirectory desktop

execute_after_dh_auto_install:
	mkdir -p debian/onionshare/usr/share/metainfo
	cp desktop/org.onionshare.OnionShare.appdata.xml debian/onionshare/usr/share/metainfo/
	mkdir -p debian/onionshare/usr/share/applications
	cp desktop/org.onionshare.OnionShare.desktop debian/onionshare/usr/share/applications/
	
	mv debian/onionshare/usr/lib/python3*/dist-packages/onionshare/resources debian/onionshare/usr/share/onionshare
	
	# Move icons to the places where they are searched
	mkdir -p debian/onionshare/usr/share/icons/hicolor/scalable/apps
	cp desktop/org.onionshare.OnionShare.svg debian/onionshare/usr/share/icons/hicolor/scalable/apps/
	$(foreach size,$(SIZES), \
	    mkdir debian/onionshare/usr/share/icons/hicolor/$(size)x$(size); \
	    mv debian/onionshare/usr/share/onionshare/onionshare-$(size).png debian/onionshare/usr/share/icons/hicolor/$(size)x$(size)/org.onionshare.OnionShare.png; \
	    ln -s /usr/share/icons/hicolor/$(size)x$(size)/org.onionshare.OnionShare.png debian/onionshare/usr/share/onionshare/onionshare-$(size).png; \
	    ) true
	
	mkdir -p debian/onionshare-cli/usr/share
	mv debian/onionshare-cli/usr/lib/python3*/dist-packages/onionshare_cli/resources debian/onionshare-cli/usr/share/onionshare-cli

override_dh_auto_test:
ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	PYBUILD_NAME=onionshare-cli HOME=/tmp dh_auto_test --buildsystem=pybuild --sourcedirectory cli

	# Pybuild export http_proxy=http://127.0.0.1:9/, https_proxy=https://127.0.0.1:9/
	# The tests are failing because of this pybuild give advice that if
	# network access is blocked by this, to export empty proxy variables.
	
	# You need to run the tests not as root as otherwise
	# some tests fail because they need chmod 400 to be effective to
	# generate an error.
	# Finally the tests need also a writeable home.
	
	$(eval TESTHOME = $(shell mktemp -d))
	$(eval ENV = QT_QPA_PLATFORM=offscreen QT_DEBUG_PLUGINS=1 HOME=$(TESTHOME) http_proxy= https_proxy=)
	PYBUILD_NAME=onionshare PYTHONPATH="$(CURDIR)/cli" pybuild \
	    --dir desktop \
	    --test --test-custom \
	    --test-args="$(ENV) xvfb-run python{version} -m pytest tests"
	rm -rf $(TESTHOME)
else
	@echo "** tests disabled"
endif
